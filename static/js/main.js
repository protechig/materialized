// Add event listener to fire when DOM is loaded - equivalent to `$(document).ready()` in jQuery
document.addEventListener( 'DOMContentLoaded', function() {

    // Dropdowns
    var dropdowns = document.querySelectorAll( '.dropdown-trigger' );
    var instances = M.Dropdown.init( dropdowns );

    //mobile menu
    var elems = document.querySelectorAll( '.sidenav' );
    var instances = M.Sidenav.init( elems );

} );


/**
 * It Reset Form
 */
function cleanForm() {
    document.getElementById( 'contact-form' ).reset();
}
